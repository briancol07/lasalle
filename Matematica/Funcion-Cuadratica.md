# Funcion Cuadratica 

Nombre | Ecuacion 
:-------:|:---------:
Polinomica | $` y = a x^2 + b x + c `$ 
Canonica  | $` y = a (x - x_{v})^2 + y_{v}`$ 
Factorizada | $` y = a (x - x_{1}) (x - x_{2})`$ 


## Buscar Datos 

Encontrar | Polinomica | Canonica | Factorizada 
----------|------------|----------|------------
Ordenada al origen | $`F(0) = c`$ | $`a x_{v}^2 + y_{v}`$ | $`a x_{1} x_{2} `$
Eje Simetria | $`\frac{-b}{2a}`$ | $` x\_{v} `$ |$` \frac{x_{1} + x_{2}}{2}`$
Ceros/Raices | $` \frac{-b \pm \sqrt{b^2 - 4 a c}}{2 a}`$ |$` \pm \frac{-y_{v}}{a} + x_{v} `$ | $` (x_{1};x_{2})`$
Vertice coordenadas | $`(\frac{-b}{2a} ; F(x_{v}))`$ | $`(x_{v};y_{v})`$ | $` (\frac{x_{1} + x_{2}}{2} ; F(x_{v})) `$


> En el vertice de coordenadas para obtener en y del vertice hay que subsituir en la ecuacion el valor de x por el valor de x del vertice que nos dio 


