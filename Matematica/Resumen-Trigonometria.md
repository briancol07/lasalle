# Resumen Trigonometria

## SOCAHTOA

### Seno 

$`sen \alpha = \frac{CO}{H}`$ 

### Coseno  

$`cos \alpha = \frac{CA}{H}`$ 

### Tangente 

$`tg \alpha = \frac{CA}{CO}`$ 

### Cosecante 

* $` cosec \alpha = \frac{1}{sen \alpha}`$
* $`cosec \alpha = \frac{H}{CO}`$ 

### Secante 

* $`sec \alpha = \frac{1}{cos \alpha}`$
* $`sec \alpha = \frac{H}{CA}`$

### Cotangente 
$`cotg \alpha = \frac{CA}{CO}`$ 

## Identidades trigonometricas 

* $` sen^2 \alpha + cos^2 \alpha = 1`$
* $` tg^2 + 1 = \frac{1}{cos^2 \alpha} `$ 
* $` 1 + cotg^2 \alpha = \frac{1}{sen^2 \alpha} `$


