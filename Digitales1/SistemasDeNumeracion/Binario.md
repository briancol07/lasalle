# Sistemas de Numeracion Binario 

> Es de base 2 significa que tiene solo dos numeros el cero y el 1 .

<br>
Un numero binario se veria de la forma : $`10101_{2}`$
<br>

## Decimal A Binario 

> Son con divisiones sucesivas por 2 para la parte entera , si es par resto entre la division es cero , si es impar el resto es 1 .


![dec-bin](./img/decimal-binario.png)

### Parte con coma 

> Para la parte con coma , se hacen multiplicaciones sucesivas x2 

![dec-bin2](./img/decimal-binario2.png)


## Codigo BCD 

> BCD = Binary Code Digit 

![BCD](./img/bcd.png)

1. Codigo Pesado/Ponderado 
   * A cada columna se le puede asignar un valor decimal determinado 
2. Espejado 
   * Los 0 cambian por 1 y los 1 por 0 
   * La suma de sus filas siempre da el maximo valor posible (primera con ultima , segunda con anteultima y asi sucesivamente) 
3. Ley de formacion 
    * $` 2^{n} `$ entonces  n = numeros de columnas 


## Octal a Binario 

> Para pasar de octal a binario es bastante simple lo unico que hay que hacer es uso del codigo BCD (la parte marcada con verde). Reemplazando cada numero por su correspondiente . 
### Ejemplo 

> El numero en octal sera el $`715.44_{8}`$

1. Primero trabajaremos con la parte entera 
  * $` 7_{8} = 111_{2}`$ 
  * $` 1_{8} = 001_{2}`$ 
  * $` 5_{8} = 101_{2}`$ 
2. Acomodamos la parte entera 
  * $` 111001101_{2} `$ 
3. Ahora hacemos la parte con coma de igual manera que con la parte entera
  * $` 4_{8} = 100_{2}`$ 
4. Acomodamos la parte con coma 
  * $` 100100_{2} `$ 
5. Ahora acomodamos todo junto 
  * $` 111001101.100100_{2} `$ 

## Hexadecimal a Binario 

> Para hexadecimal es muy parecido a octal pero lo que ocurre es que en vez de tomar lo verde tomaremos todo el codigo BCD 

### Ejemplo 

> El numero en hexadecimal sera  $`A3.27_{16}`$ recorda que luego del 10 en hexa pasa a ser A hasta la F que seria 15

1. Primero trabajaremos con la parte entera 
  * $` A_{16} = 1010_{2}`$ 
  * $` 3_{16} = 0101_{2}`$ 
2. Acomodamos la parte entera 
  * $` 10100101_{2} `$ 
3. Ahora hacemos la parte con coma de igual manera que con la parte entera
  * $` 2_{16} = 0010_{2}`$ 
  * $` 7_{16} = 0111_{2}`$ 
4. Acomodamos la parte con coma 
  * $` 00100111_{2} `$ 
5. Ahora acomodamos todo junto 
  * $` 10100101.00100111_{2} `$ 
