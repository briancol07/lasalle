# Sistema de Numeracion Decimal 

> Es de base 10 eso indica que tiene 10 numeros de 0 a 9, normalmente la base no la escribimos 

## Binario a Decimal 

Lo que vamos a hacer es a cada posicion del numero binario lo vamos a multiplicar por $`2^{posicion}`$ se empieza contando desde la coma hacia la derecha siempre comenzando de cero 

### Parte entera 

![bin-dec](./img/binario-decimal.png) 
