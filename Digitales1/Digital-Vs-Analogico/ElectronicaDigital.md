# Que es la electronica Digital ? 

La electronica digital puede definirse como la parte de la electronica que estudia los dispositivos, circuitos y sistemas digitales, binarios o logicos. 
A Diferencia de la electronica lineal o analoga que trabaja con señales que pueden adoptar una amplica gama de valores de tension. 
Los valores de tension en la digital estan limitados a adoptar uno de dos valores llamados **niveles logicos** alto(1) o bajo(0). Generalmente el nivel logico alto corresponde a la presencia de tension y el 0 o bajo a la ausencia del mismo. 


![circuito1](./img/circuito1.png)

## Circuito Analogicos Vs Digitales

Los circuitos electronicos en general se dividen en dos grandes categorias : 

* Circuitos Analogicos 
  * Reloj (con agujas) 
  * Radios 
* Circuitos Digitales
  * Reloj (Digital) 
  * Celulares

### Circuitos Analogicos 

Trabajas con una amplia variedad de señales que varian de forma continua dentro de los valores , tambien son llamados circuitos lineales 

### Circuitos digitales 

Trabajan con señales que pueden adoptar unicamente dos valores posibles.
Un circuito digital puede tener una o mas entradas, con una o mas salidas. El estados de las salidas depende de las entradas 

El bit es la unidad basica de informacion. Los circuitos digitales manejan informacion en forma de bits.

![graf1](./img/graf1.png)

## Circuito Analogico simple 

![circ2](./img/circ2.png) 

Al girar la perrilla (Regulador) podemos obtner una variacion continua de la iluminacion llevandola desde un valor minimo hasta un valor maximo. 

## Logica Digital 


### Que es la logica ? 

La logica es la aplicacion metodica de principios, reglas y criterios de razonamiento para la demostracion y derivacion de proposiciones.
Una proposicion es la expresion verbal de un juicio acerca de algo. 
En logica existen dos clases de proposiciones: 

* Simples 
  * Afirman o niegan algo 
  * la electronica es facil
* Compuestas 
  * Son la combinacion de dos o mas composiciones simples

### Silogismo 

El silogismo es un metodo de llegar a una conclusion logica a partir de dos premisas.

### Logica Digital 

La Logica digital es una ciencia de razonamiento numerico aplicada a circuitos electronicos que realizan decisiones del tipo "si , entonces" si una serie de circunstancias particulares ocurre, entonces una accion particular resulta.
La posibilidad de predecir el resultado final permite el diseño de sistemas digitales a partir de circuitos basicos llamados compuertas.Son bloques que realizan operaciones logicas sencillas y toman decisiones. De los cuales luegos salen los circuitos logicos combinatorios (mas complejos y conjunto de compuertas) 


